package com.andrew.programmingexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgrammingexamApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgrammingexamApplication.class, args);
	}

}
